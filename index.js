let studentNames = [];

function addStudent(student){
	let studentsList = studentNames.push(student);
	console.log(student + " was added to the student's list.");
}

function countStudents(){
	console.log("There are total of " + studentNames.length + " students enrolled.");
}

function printStudents(){
	studentNames.sort().forEach(function(student){
		console.log(student);
	})
}

function findStudent(filteredName){
	let studentsList = studentNames.filter(function(student){
		return student.toLowerCase().includes(filteredName.toLowerCase());
	})

	if (studentsList.length === 0){
		console.log(filteredName + " is not an enrollee.");
	}

	else if (studentsList.length === 1){
		console.log(filteredName + " is an enrollee.");
	}
	else{
		console.log(studentsList + " are enrollees.");
	}
}
